#include "ofApp.h"


int capW;
int capH;

int procImgW = 240;
int procImgH = 320;

//int procImgW = 360;
//int procImgH = 480;

vector<ColorFilter> colorFilterVec;

ofTexture testImg;




//--------------------------------------------------------------
void ofApp::setup(){
    
    ofBackground(255);
    
    ofLog(OF_LOG_WARNING);
    ofDisableArbTex(); // we need GL_TEXTURE_2D for our models coords.
    
    ofSetOrientation(OF_ORIENTATION_90_LEFT);
    //
    setupDevGUI();
    //
    redThreshold = 36;
    blueThreshold = 10;
    greenThreshold = 22;
    yellowThreshold = 22; //Dark green
    
    colorFilterVec.push_back(ColorFilter(150, redThreshold, procImgW, procImgH));//Magenta
    colorFilterVec.push_back(ColorFilter(100, blueThreshold, procImgW, procImgH));
    colorFilterVec.push_back(ColorFilter(30, greenThreshold, procImgW, procImgH));
    //    colorFilterVec.push_back(ColorFilter(20, yellowThreshold, procImgW, procImgH));
    colorFilterVec.push_back(ColorFilter(55, yellowThreshold, procImgW, procImgH)); //dark green
    //
    currentSelectedColor = -1;
    //
    
#ifndef SIMULATOR
    vidGrabber.setVerbose(true);
    vidGrabber.setup(240,320);
//    vidGrabber.initGrabber(240,320);
    
    capW = vidGrabber.getWidth();
    capH = vidGrabber.getHeight();
#else
    ofLoadImage(testImg, "testImg.png");
    
#endif
    
    cout<<"CapW: "<<capW<<" - CapH: "<<capH<<endl;
    colorImg.allocate(capW,capH);
    colorResizedImg.allocate(procImgW, procImgH);
    colorFilteredImg.allocate(procImgW, procImgH);
    colorHSVImg.allocate(procImgW, procImgH);
    redTempImg.allocate(procImgW, procImgH);
    //
    radioModules.push_back(ProductModule(speakerModuleId,
                                         "ModelParts/speaker3D.stl",
                                         "ModelParts/speakerPhysicalInfo.png",
                                         "ModelParts/speakerCircuit.png",
                                         "ModelParts/speakerBOM.png"
                                         ));
    radioModules.push_back(ProductModule(mcuModuleId,
                                         "ModelParts/mcu3D.stl",
                                         "ModelParts/mcuPhysicalInfo.png",
                                         "ModelParts/mcuCircuit_smaller.png",
                                         "ModelParts/mcuBOM.png",
                                         "ModelParts/mcuSoftware.png"
                                         ));
    radioModules.push_back(ProductModule(interfaceModuleId,
                                         "ModelParts/interface3D.stl",
                                         "ModelParts/interfacePhysicalInfo.png",
                                         "ModelParts/interfaceCircuit.png",
                                         "ModelParts/interfaceBOM.png"
                                         ));
    radioModules.push_back(ProductModule(fmModuleId,
                                         "ModelParts/fm3D.stl",
                                         "ModelParts/fmPhysicalInfo.png",
                                         "ModelParts/fmCircuit.png",
                                         "ModelParts/fmBOM.png"
                                         ));
    
    //
    resetSelections();
    //
    bConnecting = false;
    
}

//--------------------------------------------------------------
void ofApp::update(){
    
    
    //    cout<<ofGetFrameRate()<<endl;
    
    static bool bSetGui = false;
    if (!bSetGui){
        setupGUI();
        bSetGui = true;
        
        gui3DButton.setSelected(true);
    }
    
    
    
    colorFilterVec[0].threshold = redThreshold;
    colorFilterVec[1].threshold = blueThreshold;
    colorFilterVec[2].threshold = greenThreshold;
    colorFilterVec[3].threshold = yellowThreshold;
    
    
    
    bool bNewFrame = false;
#ifndef SIMULATOR
    vidGrabber.update();
    bNewFrame = vidGrabber.isFrameNew();
#else
    if (currentProductModuleId != devGuiSelectedObject){
        setProductModuleId(static_cast<ProductModuleId>(devGuiSelectedObject.get()));
    }
#endif
    if (bNewFrame){
        int nBlobs =0;
#ifndef SIMULATOR
        colorImg.setFromPixels(vidGrabber.getPixels(), capW,capH);
#endif
        cvResize(colorImg.getCvImage(),colorResizedImg.getCvImage());
        cvSmooth(colorResizedImg.getCvImage(), colorResizedImg.getCvImage());
        cvSmooth(colorResizedImg.getCvImage(), colorResizedImg.getCvImage());
        cvSmooth(colorResizedImg.getCvImage(), colorResizedImg.getCvImage());
        cvSmooth(colorResizedImg.getCvImage(), colorResizedImg.getCvImage());
        //        cvSmooth(colorResizedImg.getCvImage(), colorResizedImg.getCvImage());
        
        cvErode(colorResizedImg.getCvImage(), colorResizedImg.getCvImage());
        cvDilate(colorResizedImg.getCvImage(), colorResizedImg.getCvImage());
        cvAddWeighted(colorResizedImg.getCvImage(), 0.8, colorFilteredImg.getCvImage(), 0.2, 0, colorFilteredImg.getCvImage());
        
        
        colorHSVImg = colorFilteredImg;
        
        colorTreshold(colorHSVImg, colorFilterVec);
        
        for (int i=0; i<colorFilterVec.size(); i++){
            contourFinder.findContours(colorFilterVec[i].filteredImage, 20, (procImgH*procImgW), 10, true);	// find holes
            colorFilterVec[i].blobs = contourFinder.blobs;
            nBlobs+=contourFinder.blobs.size();
        }
        
        //Decide if there is and which is the selected product model
        if (nBlobs==0){
            currentSelectedColor = -1;
            currentProductModuleId = noneModuleId;
            guiPhysicalInfoFullScreen = false;
            guiCircuitImgFullScreen = false;
            guiBomImgFullScreen = false;
            guiSoftwareImgFullScreen = false;
            guiFullScreenActive = false;
            cout<<"Product selection lost"<<endl;
            
            resetSelections();
            
        }
    }
    
    updateGUI();
    
    
}


//Draw wireframe and areas -
void ofApp::draw(){
    drawVideoImage(0,0,0,0);
    drawGUI();
    
    //Dev gui
    if (bShowDevGUI){
        devGUI.draw();
    }
    
    ofPushStyle();
    ofColor col;
    col.setHex(0xEc8a00);
    ofSetColor(col,255*0.10);
    ofRect(0, 0, ofGetWidth(), ofGetHeight());
    ofPopStyle();
    
}

void ofApp::drawVideoImage(float x, float y, float w, float h){
    
    ofPushMatrix();
    ofPushStyle();
    if (guiFullScreenActive)
        ofSetColor(255, 40);
    
    //    colorFilteredImg.draw(X_VIDEO, Y_VIDEO, W_VIDEO, H_VIDEO);
    colorImg.draw(0, 0, ofGetWidth(), ofGetHeight());
#ifdef DEBUG
    //    contourFinder.draw(X_VIDEO, Y_VIDEO, W_VIDEO, H_VIDEO);
#endif
    
    
    if (!bConnecting){
        //Just draw the first blob. The biggest one
        for (int iColor=0; iColor<colorFilterVec.size();iColor++){
            if (colorFilterVec[iColor].blobs.size()>0){
                
                //draw outline
                vector<ofPoint> pts = colorFilterVec[iColor].blobs[0].pts;
                ofPushStyle();
                ofSetColor(ofColor::white);
                ofSetLineWidth(2);
                ofNoFill();
                ofBeginShape();
                for (int j = 0; j < pts.size(); j++){
                    pts[j]*=ofPoint((float)W_VIDEO/procImgW,(float)H_VIDEO/procImgH);
                    pts[j]+=ofPoint(X_VIDEO, Y_VIDEO);
                    ofVertex(pts[j].x, pts[j].y);
                }
                ofEndShape(true);
                ofPopStyle();
                
                //draw fill for selected
                if (iColor == currentSelectedColor){
                    ofPushStyle();
                    ofSetColor(ofColor::white, 160);
                    ofFill();
                    ofBeginShape();
                    for (int j = 0; j < pts.size(); j++){
                        ofVertex(pts[j].x, pts[j].y);
                    }
                    ofEndShape(true);
                    ofPopStyle();
                }
            }
        }
    }
    
#ifdef SIMULATOR
    testImg.draw(0, 0, ofGetWidth(), ofGetHeight());
#endif
    
    ofPopStyle();
    ofPopMatrix();
    
}

void ofApp::draw3DModel(float x, float y, float w, float h){
    
    radioModules[currentProductModuleId].draw(x, y, w, h);
    
}
void ofApp::drawGUI(){
    
    //Full screen mode
    if (guiPhysicalInfoFullScreen){
        for (int i=0; i<radioModules.size();i++){
            if (radioModules[i].productModuleId == currentProductModuleId){
                drawModeContentFullScreen(radioModules[i].physicalInfoImg);
            }
        }
        
    } else if (guiCircuitImgFullScreen) {
        for (int i=0; i<radioModules.size();i++){
            if (radioModules[i].productModuleId == currentProductModuleId){
                drawModeContentFullScreen(radioModules[i].circuitImg);
            }
        }
        
    } else if (guiBomImgFullScreen){
        for (int i=0; i<radioModules.size();i++){
            if (radioModules[i].productModuleId == currentProductModuleId){
                drawModeContentFullScreen(radioModules[i].bomImg);
            }
        }
        
    } else if (guiSoftwareImgFullScreen){
        for (int i=0; i<radioModules.size();i++){
            if (radioModules[i].productModuleId == currentProductModuleId){
                drawModeContentFullScreen(radioModules[i].softwareImg);
            }
        }
    }
    
    
    ofPushStyle();
    
    //bg
    ofFill();
    ofSetColor(guiColorBg);
    ofRect(guiX, guiY, guiWidth, guiHeight);
    ofSetColor(guiColorText);
    ofSetLineWidth(2);
    ofLine(guiX, guiY, guiX, guiY + guiHeight);
    
    //Title
    ofSetColor(guiColorText);
    drawCenterAlligned(ofPoint(guiStatusX + guiStatusWidth*0.5, guiStatusY - 20), guiMainFont, appName);
    
    //Status
    ofSetLineWidth(2);
    ofNoFill();
    ofSetColor(guiColorText);
    ofRect(guiStatusX, guiStatusY, guiStatusWidth, guiStatusHeight);
    
    ofPoint pt;
    
    //Status buttons
    gui3DButton.draw();
    guiElectronicsButton.draw();
    guiSoftwareButton.draw();
    ofPopStyle();
    
    
    //Mode
    
    //------------Draw area visual content
    if (currentMode == mode3D){
        
        for (int i=0; i<radioModules.size();i++){
            if (radioModules[i].productModuleId == currentProductModuleId){
                
                //3d model
                radioModules[i].draw(guiModeX + guiModeWidth*0.5, guiModeY + guiModeHeight*0.25, guiModeWidth, guiModeHeight*0.2);
                
                guiSave3DButton.draw();
                
                float imgRatio = (float)radioModules[i].physicalInfoImg.getWidth()/radioModules[i].physicalInfoImg.getHeight();
                if (imgRatio>1){
                    radioModules[i].physicalInfoImg.draw(guiModeLowerRect.x, guiModeLowerRect.y + (guiModeLowerRect.getHeight() - guiModeLowerRect.getHeight()/imgRatio)*0.5, guiModeLowerRect.getWidth(), guiModeLowerRect.getWidth()/imgRatio);
                }else{
                    radioModules[i].physicalInfoImg.draw(guiModeLowerRect.x + (guiModeLowerRect.getWidth() - guiModeLowerRect.getWidth()*imgRatio)*0.5, guiModeLowerRect.y, guiModeLowerRect.getHeight()*imgRatio, guiModeLowerRect.getHeight());
                }
                
            }
            
        }
        
        //draw physical info
        
        ofPushStyle();
        ofSetLineWidth(2);
        ofSetColor(guiColorText);
        ofLine(guiModeLowerRect.x, guiModeLowerRect.y, guiModeLowerRect.x + guiModeLowerRect.getWidth(), guiModeLowerRect.y);
        ofPopStyle();
        
        
    }else if (currentMode == modeElectronics){
        for (int i=0; i<radioModules.size();i++){
            if (radioModules[i].productModuleId == currentProductModuleId){
                float imgRatio = (float)radioModules[i].circuitImg.getWidth()/radioModules[i].circuitImg.getHeight();
                if (imgRatio>1){
                    radioModules[i].circuitImg.draw(guiModeUpperRect.x, guiModeUpperRect.y + (guiModeUpperRect.getHeight() - guiModeUpperRect.getHeight()/imgRatio)*0.5, guiModeUpperRect.getWidth(), guiModeUpperRect.getWidth()/imgRatio);
                }else{
                    radioModules[i].circuitImg.draw(guiModeUpperRect.x + (guiModeUpperRect.getWidth() - guiModeUpperRect.getWidth()*imgRatio)*0.5, guiModeUpperRect.y, guiModeUpperRect.getHeight()*imgRatio, guiModeUpperRect.getHeight());
                }
                
                imgRatio = (float)radioModules[i].bomImg.getWidth()/radioModules[i].bomImg.getHeight();
                if (imgRatio>1){
                    radioModules[i].bomImg.draw(guiModeLowerRect.x, guiModeLowerRect.y + (guiModeLowerRect.getHeight() - guiModeLowerRect.getHeight()/imgRatio)*0.5, guiModeLowerRect.getWidth(), guiModeLowerRect.getWidth()/imgRatio);
                }else{
                    radioModules[i].bomImg.draw(guiModeLowerRect.x + (guiModeLowerRect.getWidth() - guiModeLowerRect.getWidth()*imgRatio)*0.5, guiModeLowerRect.y, guiModeLowerRect.getHeight()*imgRatio, guiModeLowerRect.getHeight());
                }
            }
        }
        ofPushStyle();
        ofSetLineWidth(2);
        ofSetColor(guiColorText);
        ofLine(guiModeLowerRect.x, guiModeLowerRect.y, guiModeLowerRect.x + guiModeLowerRect.getWidth(), guiModeLowerRect.y);
        ofPopStyle();
        
    }else if (currentMode == modeSoftware){
        for (int i=0; i<radioModules.size();i++){
            if (radioModules[i].productModuleId == currentProductModuleId){
                float imgRatio = (float)radioModules[i].softwareImg.getWidth()/radioModules[i].softwareImg.getHeight();
                radioModules[i].softwareImg.draw(
                                                 guiModeFullRect.x,
                                                 guiModeFullRect.y,
                                                 guiModeFullRect.getWidth(),
                                                 guiModeFullRect.getWidth()/imgRatio
                                                 );
                
                
            }
        }
        
    }
    
    //Mode - outer rect
    ofPushStyle();
    ofSetLineWidth(2);
    ofNoFill();
    ofSetColor(guiColorText);
    ofRect(guiModeX, guiModeY, guiModeWidth, guiModeHeight);
    ofPopStyle();
    
    
    
    //Status text
    statusText.draw();
    
    //
    savingAlert.draw();
    
    
    
}

void ofApp::drawModeContentFullScreen(ofImage &modeContentImg){
    ofPoint pos = ofPoint(
                          ofGetWidth()*0.5 - modeContentImg.getWidth()*0.5,
                          ofGetHeight()*0.5 - modeContentImg.getHeight()*0.5
                          );
    
    //    modeContentImg.draw(pos);
    float imgRatio = (float)modeContentImg.getWidth()/modeContentImg.getHeight();
    
    scrollableAreaManager.apply();
    
    if (scrollableAreaRect.getWidth()/imgRatio > scrollableAreaRect.getHeight()){
        modeContentImg.draw(0,0,scrollableAreaRect.getWidth(), scrollableAreaRect.getWidth()/imgRatio);
        scrollableAreaManager.cam.setViewportConstrain(
                                                       ofVec3f(-scrollableAreaRect.x, -scrollableAreaRect.y),
                                                       ofVec3f(
                                                               scrollableAreaRect.getWidth() + (ofGetWidth() - scrollableAreaRect.getWidth()) - scrollableAreaRect.x,
                                                               scrollableAreaRect.getWidth()/imgRatio)
                                                       ); //limit browseable area, in world units
        
    }else{
        scrollableAreaManager.cam.setViewportConstrain(
                                                       ofVec3f(-scrollableAreaRect.x, -scrollableAreaRect.y),
                                                       ofVec3f(
                                                               scrollableAreaRect.getWidth() + (ofGetWidth() - scrollableAreaRect.getWidth()) - scrollableAreaRect.x,
                                                               scrollableAreaRect.getHeight())
                                                       ); //limit browseable area, in world units
        modeContentImg.draw(0,(float)scrollableAreaRect.getHeight()*0.5 - (float)modeContentImg.getHeight()*0.5,scrollableAreaRect.getWidth(), scrollableAreaRect.getWidth()/imgRatio);
    }
    scrollableAreaManager.reset();
    
    
    
    guiCloseFullScreenButton.x = guiPadding;
    guiCloseFullScreenButton.y = guiPadding;
    guiCloseFullScreenButton.draw();
    
}


//--------------------------------------------------------------
void ofApp::exit(){
    
}

//--------------------------------------------------------------
void ofApp::touchDown(ofTouchEventArgs & touch){
    if (!guiFullScreenActive)
        touchDownGUI(touch);
    else{
        guiCloseFullScreenButton.updateTouchDown(touch);
        scrollableAreaManager.touchDown(touch);
    }
}

//--------------------------------------------------------------
void ofApp::touchMoved(ofTouchEventArgs & touch){
    if (guiFullScreenActive)
        scrollableAreaManager.touchMoved(touch);
}

//--------------------------------------------------------------
void ofApp::touchUp(ofTouchEventArgs & touch){
    if (!guiFullScreenActive){
        if (!touchUpGUI(touch))
            selectObjectModel(touch);
        
        if (touch.numTouches == 2){
            bShowDevGUI = !bShowDevGUI;
        }
    }else{
        scrollableAreaManager.touchUp(touch);
        if(guiCloseFullScreenButton.updateTouchUp(touch)){
            guiPhysicalInfoFullScreen = false;
            guiCircuitImgFullScreen = false;
            guiBomImgFullScreen = false;
            guiSoftwareImgFullScreen = false;
            guiFullScreenActive = false;
            scrollableAreaManager.setZoom(1);
        }
    }
    
    
}

void ofApp::touchDownGUI(ofPoint _fingerPos){
    if (gui3DButton.inside(_fingerPos.x, _fingerPos.y))
        gui3DButton.updateTouchDown(_fingerPos);
    if (guiElectronicsButton.inside(_fingerPos.x, _fingerPos.y))
        guiElectronicsButton.updateTouchDown(_fingerPos);
    if (guiSoftwareButton.inside(_fingerPos.x, _fingerPos.y))
        guiSoftwareButton.updateTouchDown(_fingerPos);
    
    if (guiSave3DButton.inside(_fingerPos))
        guiSave3DButton.updateTouchDown(_fingerPos);
    
}
bool ofApp::touchUpGUI(ofPoint _fingerPos){
    
    
    if (gui3DButton.updateTouchUp(_fingerPos)){
        setMode(mode3D);
        gui3DButton.setSelected(false);
        guiElectronicsButton.setSelected(false);
        guiSoftwareButton.setSelected(false);
        gui3DButton.setSelected(true);
    }
    else if (guiElectronicsButton.updateTouchUp(_fingerPos)){
        setMode(modeElectronics);
        gui3DButton.setSelected(false);
        guiElectronicsButton.setSelected(false);
        guiSoftwareButton.setSelected(false);
        guiElectronicsButton.setSelected(true);
    }
    else if (guiSoftwareButton.updateTouchUp(_fingerPos)){
        setMode(modeSoftware);
        gui3DButton.setSelected(false);
        guiElectronicsButton.setSelected(false);
        guiSoftwareButton.setSelected(false);
        guiSoftwareButton.setSelected(true);
    }
    else if (guiSave3DButton.updateTouchUp(_fingerPos)){
        savingAlert.start();
    }
    else if (guiModeUpperRect.inside(_fingerPos)){
        if (currentMode == modeElectronics){
            guiCircuitImgFullScreen = true;
            guiFullScreenActive = true;
        }else if (currentMode == modeSoftware){
            guiSoftwareImgFullScreen = true;
            guiFullScreenActive = true;
        }
    }
    
    else if (guiModeLowerRect.inside(_fingerPos)){
        if (currentMode == mode3D){
            guiPhysicalInfoFullScreen = true;
            guiFullScreenActive = true;
        }
        else if (currentMode == modeElectronics){
            guiBomImgFullScreen = true;
            guiFullScreenActive = true;
        }else if (currentMode == modeSoftware){
            guiSoftwareImgFullScreen = true;
            guiFullScreenActive = true;
        }
    }
    
    
    
    if (ofRectangle(guiX, guiY, guiWidth, guiHeight).inside(_fingerPos))
        return true;
    return false;
    
}



//--------------------------------------------------------------
void ofApp::touchDoubleTap(ofTouchEventArgs & touch){
    
}

//--------------------------------------------------------------
void ofApp::touchCancelled(ofTouchEventArgs & touch){
    
}

//--------------------------------------------------------------
void ofApp::lostFocus(){
    
}

//--------------------------------------------------------------
void ofApp::gotFocus(){
    
}

//--------------------------------------------------------------
void ofApp::gotMemoryWarning(){
    
}

//--------------------------------------------------------------
void ofApp::deviceOrientationChanged(int newOrientation){
    
}

void ofApp::setupDevGUI(){
    
    // change default sizes for ofxGui so it's usable in small/high density screens
    //    ofxGuiSetFont("Questrial-Regular.ttf",10,true,true);
    ofxGuiSetTextPadding(4);
    ofxGuiSetDefaultWidth(300);
    ofxGuiSetDefaultHeight(18);
    
    devGUI.setup("panel"); // most of the time you don't need a name but don't forget to call setup
    devGUI.add(redThreshold.set("Red thresh.", 20, 1, 50));
    devGUI.add(blueThreshold.set("Blue thresh.", 20, 1, 50));
    devGUI.add(greenThreshold.set("Green thresh.", 20, 1, 50));
    devGUI.add(yellowThreshold.set("Yellow thresh.", 20, 1, 50));
    
    devGUI.add(devGuiSelectedObject.set("Selected object", 0, 0, 4));
    
    bShowDevGUI = false;
}

void ofApp::resetSelections(){
    currentProductModuleId = noneModuleId;
    currentSelectedColor = -1;
    guiSoftwareButton.setInactive(true);
    gui3DButton.setInactive(true);
    guiElectronicsButton.setInactive(true);
    //
    currentMode = mode3D;
    
}

void ofApp::colorTreshold(ofxCvColorImage &sourceImg, vector<ColorFilter> &colorFilter){
    
    sourceImg.convertRgbToHsv();
    
    //    //Red - around 0
    //    cvInRangeS(
    //               sourceImg.getCvImage(),
    //               cvScalar(0,100,100),
    //               cvScalar(0 + colorFilterVec[0].threshold*0.5,255,255),
    //               colorFilterVec[0].filteredImage.getCvImage()
    //               );
    //
    //    cvInRangeS(
    //               sourceImg.getCvImage(),
    //               cvScalar(179 - colorFilterVec[0].threshold*0.5,100,100),
    //               cvScalar(179,255,255),
    //               redTempImg.getCvImage()
    //               );
    //
    //    cvOr(redTempImg.getCvImage(), colorFilterVec[0].filteredImage.getCvImage(), colorFilterVec[0].filteredImage.getCvImage());
    //
    //other colors
    for (int i=0; i<colorFilterVec.size() - 1; i++){
        cvInRangeS(
                   sourceImg.getCvImage(),
                   cvScalar(colorFilterVec[i].colorHue,100,100),
                   cvScalar(colorFilterVec[i].colorHue + colorFilterVec[i].threshold,255,255),
                   colorFilterVec[i].filteredImage.getCvImage()
                   );
    }
    
    //Dark Green
    cvInRangeS(
               sourceImg.getCvImage(),
               cvScalar(colorFilterVec[colorFilterVec.size() - 1].colorHue,100,40),
               cvScalar(colorFilterVec[colorFilterVec.size() - 1].colorHue + colorFilterVec[colorFilterVec.size() - 1].threshold,255,255),
               colorFilterVec[colorFilterVec.size() - 1].filteredImage.getCvImage()
               );
    
    
    
    
    //    //Green - around 60
    //    cvInRangeS(sourceImg.getCvImage(), cvScalar(100,100,100),cvScalar(120,255,255),colorFilterVec[1].filteredImage.getCvImage());
    //
    //    //Blue - around 120
    //    cvInRangeS(sourceImg.getCvImage(), cvScalar(100,100,100),cvScalar(120,255,255),colorFilterVec[2].filteredImage.getCvImage());
    //
    //    //Yellow - around 30
    //    cvInRangeS(sourceImg.getCvImage(), cvScalar(20,100,100),cvScalar(30,255,255),colorFilterVec[3].filteredImage.getCvImage());
    
}

//--------------------------------------------------------------
void ofApp::setupGUI(){
    
    appName = "HACKING HOUSEHOLDS / INSPECTOR";
    
    
    
    guiHeight = ofGetHeight();
    guiWidth = 664;
    guiX = (ofGetWidth() - guiWidth);
    guiY = 0;
    guiPadding = 30;
    
    guiStatusX = guiX + guiPadding;
    guiStatusY = guiY + guiPadding*3;
    guiStatusHeight = 150;
    guiStatusWidth = guiWidth - 2*(guiPadding);
    
    
    guiModeX = guiX + guiPadding;
    guiModeY = guiStatusY + guiStatusHeight + guiPadding*4;
    guiModeHeight = ofGetHeight() - guiModeY - guiPadding;
    guiModeWidth = guiWidth - 2*(guiPadding);
    
    guiModeUpperRect = ofRectangle(guiModeX, guiModeY, guiModeWidth, guiModeHeight*0.5);
    guiModeLowerRect = ofRectangle(guiModeX, guiModeY + guiModeHeight*0.5, guiModeWidth, guiModeHeight*0.5);
    guiModeFullRect = ofRectangle(guiModeX, guiModeY, guiModeWidth, guiModeHeight);
    
    
    ofTrueTypeFont::setGlobalDpi(72);
    guiMainFont.load("Inconsolata.ttf", 34);

    
    guiButtonFont.load("Inconsolata.ttf", 23);
    
    gui3DButton.setText(guiButtonFont, "3D");
    gui3DButton.width = 157;
    gui3DButton.height = 40;
    gui3DButton.x = guiModeX + guiPadding;
    gui3DButton.y = guiModeY - gui3DButton.getHeight() - guiPadding/2;
    
    
    guiElectronicsButton = gui3DButton;
    guiElectronicsButton.x+=gui3DButton.getWidth() + guiPadding;
    guiElectronicsButton.setText(guiButtonFont, "Electronics");
    
    guiSoftwareButton = guiElectronicsButton;
    guiSoftwareButton.x+=guiElectronicsButton.getWidth() + guiPadding;
    guiSoftwareButton.width = (guiStatusX + guiStatusWidth - (guiElectronicsButton.x + guiElectronicsButton.width)) - 2*guiPadding;
    guiSoftwareButton.setText(guiButtonFont, "Software");
    
    guiCloseFullScreenButton.width = guiPadding*2;
    guiCloseFullScreenButton.height = guiPadding*2;
    
    //
    guiStatusFont.load("Inconsolata.ttf", 24);
    guiStatusFont.setLineHeight(25);
    statusText = StatusText(guiStatusX, guiStatusY, guiStatusWidth, guiStatusHeight);
    statusText.setText(guiStatusFont);
    statusText.start();
    
    guiPhysicalInfoFullScreen = false;
    guiCircuitImgFullScreen = false;
    guiBomImgFullScreen = false;
    guiSoftwareImgFullScreen = false;
    guiFullScreenActive = false;
    
    scrollableAreaRect = ofRectangle(0, 0, guiX, ofGetHeight());
    scrollableAreaManager.setup(scrollableAreaRect);
    
    savingAlert = SavingAlert(guiStatusX, guiStatusY, guiStatusWidth, guiStatusHeight);
    savingAlert.setText(guiStatusFont);
    savingAlert.width = 500;
    savingAlert.height = 180;
    savingAlert.x = scrollableAreaRect.getWidth()*0.5 - savingAlert.getWidth()*0.5;
    savingAlert.y = scrollableAreaRect.getHeight()*0.5 - savingAlert.getHeight()*0.5;
    
    guiSave3DButton.width = guiPadding*1.5;
    guiSave3DButton.height = guiPadding*1.5;
    guiSave3DButton.x = guiModeUpperRect.x + guiModeUpperRect.getWidth() - guiSave3DButton.getWidth() - 4;
    guiSave3DButton.y = guiModeUpperRect.y + guiModeUpperRect.getHeight() - guiSave3DButton.getHeight() - 4;
    
    //    //Setup scrollviews
    //    circuitImgScrollView.setWindowRect(guiModeUpperRect);
    //    setupScrollViewDefaults(circuitImgScrollView);
    //    circuitImgScrollView.setup();
    //
    //    ofRectangle bomWindow;
    //    bomScrollView.setWindowRect(bomWindow);
    //
    //    ofRectangle softwareWindow;
    //    softwareScrollView.setWindowRect(softwareWindow);
    //
    //    ofRectangle physicalInfoWindow;
    //    physicalInfoScrollView.setWindowRect(physicalInfoWindow);
    
}

//void ofApp::setupScrollViewDefaults(ofxScrollView &scrollView){
//    scrollView.fitContentToWindow(OF_ASPECT_RATIO_KEEP_BY_EXPANDING); // fits content into window, works with ofAspectRatioMode values.
//    scrollView.setScrollEasing(0.3); // smoothness of scrolling, between 0 and 1.
//    scrollView.setBounceBack(0.3); // the speed of bounce back, between 0 and 1.
//    scrollView.setDragVelocityDecay(0.9); // the speed of decay of drag velocity after release, between 0 and 1.
//    scrollView.setUserInteraction(true); // enable / disable mouse or touch interaction.
//
//    scrollView.setDoubleTapZoom(true); // enable double tap zoom.
//    scrollView.setDoubleTapZoomIncrement(1.0); // the increment value of zoom on double tap, between 0 and 1.
//    scrollView.setDoubleTapZoomIncrementTimeInSec(0.3); // the time amount of time for zoom increment.
//    scrollView.setDoubleTapRegistrationTimeInSec(0.25); // the time threshold between taps for double tap event to register.
//    scrollView.setDoubleTapRegistrationDistanceInPixels(20); // the distance threshold between taps for double tap event to register.
//}

//--------------------------------------------------------------
void ofApp::updateGUI(){
    
    scrollableAreaManager.update();
    
    
    for (int i=0; i<radioModules.size();i++){
        if (radioModules[i].productModuleId == currentProductModuleId){
            radioModules[i].bSaving = savingAlert.isShown();
        }
    }
    
    //    circuitImgScrollView.update();
}

void ofApp::setMode(InspectorMode newMode){
    if (newMode != currentMode){
        cout<<"Mode changed to: "<<newMode<<endl;
        currentMode = newMode;
        
        
        //        if (currentMode == mode3D){
        //            ofRectangle physicalInfoContent;
        //            physicalInfoContent.getHeight() = radioModules[currentProductModuleId].physicalInfoImg.getHeight();
        //            physicalInfoContent.getWidth() = radioModules[currentProductModuleId].physicalInfoImg.getWidth();
        //            physicalInfoScrollView.setContentRect(physicalInfoContent);
        //            physicalInfoScrollView.setup();
        //        }else if (currentMode == modeSoftware){
        //            ofRectangle softwareContent;
        //            softwareContent.getHeight() = radioModules[currentProductModuleId].softwareImg.getHeight();
        //            softwareContent.getWidth() = radioModules[currentProductModuleId].softwareImg.getWidth();
        //            softwareScrollView.setContentRect(softwareContent);
        //            softwareScrollView.setup();
        //        }else if (currentMode == modeElectronics){
        //            ofRectangle bomContent;
        //            bomContent.getHeight() = radioModules[currentProductModuleId].bomImg.getHeight();
        //            bomContent.getWidth() = radioModules[currentProductModuleId].bomImg.getWidth();
        //            bomScrollView.setContentRect(bomContent);
        //            bomScrollView.setup();
        //
        //            ofRectangle circuitContent;
        //            circuitContent.getHeight() = radioModules[currentProductModuleId].circuitImg.getHeight();
        //            circuitContent.getWidth() = radioModules[currentProductModuleId].circuitImg.getWidth();
        //            circuitImgScrollView.setContentRect(circuitContent);
        //            circuitImgScrollView.setup();
        //        }
        //        circuitImgScrollView.setup();
        //        bomScrollView.setup();
        //        softwareScrollView.setup();
        //        
        //
    }
    //do setup if needed
}


void ofApp::selectObjectModel(ofPoint _fingerPos){
    
    ofPoint fingerPos = ofPoint(_fingerPos.x, _fingerPos.y);
    fingerPos-=ofPoint(X_VIDEO, Y_VIDEO);
    fingerPos*=ofPoint((float)procImgW/(W_VIDEO),(float)procImgH/(H_VIDEO));
    
    for (int iColor=0; iColor<colorFilterVec.size(); iColor++){
        for (int iBlob=0; iBlob<colorFilterVec[iColor].blobs.size();iBlob++){
            if (hitTest(colorFilterVec[iColor].blobs[iBlob].pts, fingerPos)){
                currentSelectedColor = iColor;
                cout<<"HIT: ";
                if (iColor == 0){
                    cout<<"Magenta - Interface module"<<endl;
                    setProductModuleId(interfaceModuleId);
                }else if (iColor == 1){
                    cout<<"Blue - MCU Module"<<endl;
                    setProductModuleId(mcuModuleId);
                }else if (iColor == 2){
                    cout<<"Green - FM module"<<endl;
                    setProductModuleId(fmModuleId);
                }else if (iColor == 3){
                    cout<<"Dark green - Speaker Module"<<endl;
                    setProductModuleId(speakerModuleId);
                }
            }
        }
    }
    statusText.setCurrentProductModule(currentProductModuleId);
}

bool ofApp::hitTest(vector<ofPoint> &poly, ofPoint hitter){
    
    int i, j, c = 0;
    for (i = 0, j = poly.size()-1; i < poly.size(); j = i++) {
        if ( ((poly[i].y>hitter.y) != (poly[j].y>hitter.y)) &&
            (hitter.x < (poly[j].x-poly[i].x) * (hitter.y-poly[i].y) / (poly[j].y-poly[i].y) + poly[i].x) )
            c = !c;
    }
    return c;
}

void ofApp::setProductModuleId(ProductModuleId newProductModuleId){
    cout<<"Selected module id changed to: "<<newProductModuleId<<endl;
    currentProductModuleId = newProductModuleId;
    setMode(mode3D);
    gui3DButton.setSelected(true);
    guiSoftwareButton.setSelected(false);
    guiElectronicsButton.setSelected(false);
    
    
    for (int i=0; i<radioModules.size();i++){
        if (radioModules[i].productModuleId == currentProductModuleId){
            if (radioModules[i].isSoftwarePresent()){
                guiSoftwareButton.setInactive(false);
            }else{
                guiSoftwareButton.setInactive(true);
            }
            gui3DButton.setInactive(false);
            guiElectronicsButton.setInactive(false);
        }
    }
    
    //setup content view
}


