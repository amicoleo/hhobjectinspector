//
//  ProductModule.h
//  hhObjectInspector
//
//  Created by Leonardo Amico on 02/09/2015.
//
//

/***

 This is responsible for loading and displaying each part of the object. 
 
 

***/


#ifndef __hhObjectInspector__ProductModule__
#define __hhObjectInspector__ProductModule__

#include "ofxAssimpModelLoader.h"
#include "ofVboMesh.h"
//#include "glu.h"
//#include "ofxScrollView.h"

enum ProductModuleId{
    noneModuleId,
    speakerModuleId,
    mcuModuleId,
    interfaceModuleId,
    fmModuleId,
};

const string productName = "hh digital radio v0.2";
static string productModuleIdToName(ProductModuleId productModuleId){
    if (productModuleId == speakerModuleId){
        return "Speaker Module";
    }else if (productModuleId == mcuModuleId){
        return "MCU Module";
    }else if (productModuleId == fmModuleId){
        return "FM Module";
    }else if (productModuleId == interfaceModuleId){
        return "Interface Module";
    }
    return "";
}



class ProductModule{
public:
    ProductModule(
            ProductModuleId _productModuleId,
            string threeDModelFilename,
            string physicalInfoFilename,
            string circuitImgFilename,
            string bomImgFilename,
            string softwareImgFilename = "");
    void draw(float x, float y, float w, float h);
    bool isSoftwarePresent();

    
    ProductModuleId productModuleId;
    
    //--3D
    ofxAssimpModelLoader threeDModel;
    ofImage physicalInfoImg;
    
    //
    
    //--Electronics
    ofImage circuitImg; //scrollable/zommable
    ofImage bomImg;
    
    
    //Software
    ofImage softwareImg;
    
    bool bSaving;
    
private:
    int rotDegree;
    bool bSoftwarePresent;
    
    
    
    
};

#endif /* defined(__hhObjectInspector__ProductModule__) */
