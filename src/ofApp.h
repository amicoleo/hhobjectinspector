//#define SIMULATOR

#pragma once

#include "ofxiOS.h"
#include "defines.h"
#include "ofxOpenCv.h"
#include "ofxAssimpModelLoader.h"
#include "ofxGui.h"
#include "ofVboMesh.h"

#include "ScrollableArea.h"
#include "ProductModule.h"

static void drawCenterAlligned(ofPoint center, ofTrueTypeFont &font, string text){
    ofRectangle boundBox = font.getStringBoundingBox(text, 0, 0);
    ofPoint begin;
    begin.x = center.x - boundBox.width*0.5;
    begin.y = center.y;
    
    font.drawString(text, begin.x, begin.y);
}

class Button : public ofRectangle{
public:
    Button():ofRectangle(0,0,0,0){
        bInactive = false;
        bTouchDown = false;
        bSelected = false;
        
    }
    void setSelected(bool _bSelected){
        bSelected = _bSelected;
    }
    void setText(ofTrueTypeFont &_font, string _text){
        text = _text;
        font = _font;
    }
    void draw(){
        ofPushStyle();
        ofSetLineWidth(2);
        ofNoFill();
        if (bInactive){
            ofSetColor(guiColorTextUnactive);
        }else{
            ofSetColor(guiColorText);
            if (bTouchDown){
                ofPushStyle();
                ofFill();
                ofSetColor(guiColorTextUnactive);
                ofRect(*this);
                ofPopStyle();
            }
        }
        if (bSelected)
            ofRect(*this);
        
        drawCenterAlligned(this->getCenter()+ofPoint(0,4), font, text);
        
        ofPopStyle();
    }
    void setInactive(bool _bInactive){
        bInactive = _bInactive;
    };
    void updateTouchDown(ofPoint fingerPos){
        if (!bInactive){
            if (this->inside(fingerPos))
                bTouchDown = true;
            else
                bTouchDown = false;
        }
    }
    
    bool updateTouchUp(ofPoint fingerPos){
        if (!bInactive){
            bool bReturnVal = false;
            if (bTouchDown){
                bReturnVal = this->inside(fingerPos);
            }
            bTouchDown = false;
            return bReturnVal;
        }
        return false;
    };
    
protected:
    bool bTouchDown;
    bool bSelected;
    ofTrueTypeFont font;
    bool bInactive;
    string text;
};

class CloseWindowButton: public Button{
public:
    CloseWindowButton():Button(){}
    void draw(){
        ofPushStyle();
        ofNoFill();
        ofSetLineWidth(2);
        ofSetColor(guiColorText);
        ofRect(*this);
        if (bInactive){
            ofSetColor(guiColorTextUnactive);
        }else{
            ofSetColor(guiColorText);
            if (bTouchDown){
                ofPushStyle();
                ofFill();
                ofSetColor(guiColorTextUnactive);
                ofRect(*this);
                ofPopStyle();
            }
        }
        if (bSelected)
            ofRect(*this);
        
        ofLine(this->x, this->y, this->x + this->width, this->y + this->height);
        ofLine(this->x + this->width, this->y, this->x, this->y + this->height);
        
        ofPopStyle();
    }
    
};


class DownloadButton: public CloseWindowButton{
public:
    DownloadButton():CloseWindowButton(){}
    void draw(){
        ofPushStyle();
        ofNoFill();
        ofSetLineWidth(2);
        ofSetColor(guiColorText);
        ofRect(*this);
        if (bInactive){
            ofSetColor(guiColorTextUnactive);
        }else{
            ofSetColor(guiColorText);
            if (bTouchDown){
                ofPushStyle();
                ofFill();
                ofSetColor(guiColorTextUnactive);
                ofRect(*this);
                ofPopStyle();
            }
        }
        if (bSelected)
            ofRect(*this);
        
        
        ofLine(this->x + this->width*0.5, this->y, this->x + this->width*0.5, this->y + this->height);
        ofLine(this->x, this->y + this->height*0.5, this->x + this->width*0.5, this->y + this->height);
        ofLine(this->x + this->width, this->y + this->height*0.5, this->x + this->width*0.5, this->y + this->height);
        
        
        ofPopStyle();
        
    }
};



enum StatusId {
    statusLoading,
    statusModuleNames
};

class StatusText:public ofRectangle{
public:
    StatusText():ofRectangle(0, 0, 0, 0){}
    StatusText(float x, float y, float w, float h):ofRectangle(x, y, w, h){
        currentAnimationFrame = 0;
        statusId = statusModuleNames;
        productModuleId = noneModuleId;
        
    }
    void setText(ofTrueTypeFont &_font){
        font = _font;
    }
    void start(){
        animationFrameInc=0.07;
    }
    void stop(){
        animationFrameInc=0;
    }
    void setCurrentStatus(StatusId _statusId){
        statusId = _statusId;
    }
    void setCurrentProductModule(ProductModuleId _productModuleId){
        productModuleId = _productModuleId;
    }
    
    void draw(){
        ofPushStyle();
        if (statusId == statusLoading){
            stringstream loadingTextss;
            loadingTextss<<loadingText;
            for (int i=0; i<(int)currentAnimationFrame%4; i++){
                loadingTextss<<".";
            }
            
            ofSetColor(guiColorText);
            //            ofPoint stringPos = this->getCenter()+ofPoint(0,4);
            ofPoint stringPos = this->position+ofPoint(16,font.getSize()+8);
            font.drawString(loadingTextss.str(), stringPos.x, stringPos.y);
        }else if (statusId == statusModuleNames){
            stringstream moduleTextss;
            moduleTextss<<productName<<endl;
            
            moduleTextss<<"   ";
            if (productModuleId == speakerModuleId)
                moduleTextss<<"> ";
            moduleTextss<<productModuleIdToName(speakerModuleId)<<endl;
            
            moduleTextss<<"   ";
            if (productModuleId == fmModuleId)
                moduleTextss<<"> ";
            moduleTextss<<productModuleIdToName(fmModuleId)<<endl;
            
            moduleTextss<<"   ";
            if (productModuleId == mcuModuleId)
                moduleTextss<<"> ";
            moduleTextss<<productModuleIdToName(mcuModuleId)<<endl;
            
            moduleTextss<<"   ";
            if (productModuleId == interfaceModuleId)
                moduleTextss<<"> ";
            moduleTextss<<productModuleIdToName(interfaceModuleId)<<endl;
            
            
            ofSetColor(guiColorText);
            ofPoint stringPos = this->position+ofPoint(16,font.getSize()+8);
            font.drawString(moduleTextss.str(), stringPos.x, stringPos.y);
            
        }
        ofPopStyle();
        
        currentAnimationFrame+=animationFrameInc;
    }
    
    
protected:
    StatusId statusId;
    ProductModuleId productModuleId;
    float  currentAnimationFrame;
    float animationFrameInc;
    
    ofTrueTypeFont font;
    
    string loadingText="Connecting";
    
};

class SavingAlert:public StatusText{
public:
    SavingAlert(){
    }
    
    SavingAlert(float x, float y, float w, float h):StatusText(x,y,w,h){
        bShow = false;
    }
    void draw(){
        
        if (bShow){
            stringstream savingTextss;
            savingTextss<<savingText;
            for (int i=0; i<(int)currentAnimationFrame%4; i++){
                savingTextss<<".";
            }
            
            currentAnimationFrame+=animationFrameInc;
            
            ofPushStyle();
            ofSetColor(guiColorBg);
            ofRect(*this);
            
            ofSetColor(guiColorText);
            ofPoint stringPos = this->position+ofPoint(16,font.getSize()+8);
            
            
            if (currentAnimationFrame < 15){
                drawCenterAlligned(this->getCenter(), font, savingTextss.str());
                
            }else if (currentAnimationFrame < 20){
                drawCenterAlligned(this->getCenter(), font, savingTextDone);
            }else{
                bShow = false;
            }
            
            ofPopStyle();
            
        }
        
        
    }
    void start(){
        animationFrameInc=0.07;
        bShow = true;
        currentAnimationFrame = 0;
        
    }
    
    bool isShown(){
        return bShow;
    }
    
private:
    float  currentAnimationFrame;
    float animationFrameInc;
    bool bShow;
    string savingText = "hhRadioMCUModule.stl\nsaving file";
    string savingTextDone = "hhRadioMCUModule.stl\nsaved successfully!";
    
};


class ColorFilter{
public:
    ColorFilter(int _colorHue, float _treshold, float w, float h){
        colorHue = _colorHue;
        threshold = _treshold;
        filteredImage.allocate(w, h);
        filteredImage.set(0);
    }
    ofxCvGrayscaleImage filteredImage;
    vector <ofxCvBlob> blobs;
    int colorHue;
    float threshold;
};

class ofApp : public ofxiOSApp{
    
public:
    void setup();
    void update();
    void draw();
    void exit();
    
    void touchDown(ofTouchEventArgs & touch);
    void touchMoved(ofTouchEventArgs & touch);
    void touchUp(ofTouchEventArgs & touch);
    void touchDoubleTap(ofTouchEventArgs & touch);
    void touchCancelled(ofTouchEventArgs & touch);
    
    void lostFocus();
    void gotFocus();
    void gotMemoryWarning();
    void deviceOrientationChanged(int newOrientation);
    
    void colorTreshold(ofxCvColorImage &sourceImg, vector<ColorFilter> &colorFilter);
    bool hitTest(vector<ofPoint> &poly, ofPoint hitter);
    void selectObjectModel(ofPoint _fingerPos);
    
    void drawVideoImage(float x, float y, float w, float h);
    void draw3DModel(float x, float y, float w, float h);
    
    void setupDevGUI();
    ofxPanel devGUI;
    ofParameter<int> redThreshold;
    ofParameter<int> blueThreshold;
    ofParameter<int> greenThreshold;
    ofParameter<int> yellowThreshold;
    ofParameter<int> devGuiSelectedObject;
    bool bShowDevGUI;
    
    
#ifndef SIMULATOR
    ofVideoGrabber 		vidGrabber;
#endif
    
    ofxCvColorImage			colorImg;
    ofxCvColorImage			colorResizedImg;
    ofxCvColorImage			colorFilteredImg;
    ofxCvColorImage			colorHSVImg;
    ofxCvGrayscaleImage		redTempImg;
    
    ofxCvContourFinder 	contourFinder;
    
    
    //
    enum InspectorMode{
        mode3D,
        modeElectronics,
        modeSoftware
    };
    void setMode(InspectorMode newMode);
    InspectorMode currentMode;
    
    //
    
    void setProductModuleId(ProductModuleId newProductModuleId);
    ProductModuleId currentProductModuleId;
    
    int currentSelectedColor;

    //
    vector <ProductModule> radioModules;
    
    ofxAssimpModelLoader model;
    
    //
    StatusText statusText;
    
    //
    void setupGUI();
    void updateGUI();
    void drawGUI();
    bool touchUpGUI(ofPoint _fingerPos);
    void touchDownGUI(ofPoint _fingerPos);
    
    
    float guiHeight;
    float guiWidth;
    float guiX;
    float guiY;
    float guiPadding;
    
    float guiStatusX;
    float guiStatusY;
    float guiStatusHeight;
    float guiStatusWidth;
    
    float guiModeX;
    float guiModeY;
    float guiModeHeight;
    float guiModeWidth;
    
    ofRectangle guiModeUpperRect;
    ofRectangle guiModeLowerRect;
    ofRectangle guiModeFullRect;
    
    Button gui3DButton, guiElectronicsButton, guiSoftwareButton;
    CloseWindowButton guiCloseFullScreenButton;
    ofTrueTypeFont guiMainFont, guiButtonFont, guiStatusFont;
    
    void drawModeContentFullScreen(ofImage& modeContentImg);
    bool guiPhysicalInfoFullScreen, guiCircuitImgFullScreen, guiBomImgFullScreen, guiSoftwareImgFullScreen;
    bool guiFullScreenActive;
    ofRectangle scrollableAreaRect;
    ScrollableArea scrollableAreaManager;
    
    string appName = "HACKING HOUSEHOLDS / INSPECTOR";
    float timeStartConnectingProcess;
    bool bConnecting;
    void resetSelections();
    
    DownloadButton guiSave3DButton;
    SavingAlert savingAlert;





};




