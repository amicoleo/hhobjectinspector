//
//  ProductModule.cpp
//  hhObjectInspector
//
//  Created by Leonardo Amico on 02/09/2015.
//
//

#include "ProductModule.h"

ProductModule::ProductModule(ProductModuleId _productModuleId, string threeDModelFilename, string physicalInfoFilename, string circuitImgFilename,string bomImgFilename, string softwareImgFilename){
    
    productModuleId = _productModuleId;
    
    //--3D
    threeDModel.loadModel(threeDModelFilename);
    ofLoadImage(physicalInfoImg, physicalInfoFilename);
    
    //
    
    //--Bom
    //Materials image (scrollable) - contains list of bits
    //Circuit image
    ofLoadImage(circuitImg, circuitImgFilename);
    ofLoadImage(bomImg, bomImgFilename);
    
    
    //Code
    ofLoadImage(softwareImg, softwareImgFilename);
    
    rotDegree = 0;
    
    if (softwareImgFilename ==""){
        bSoftwarePresent = false;
    }else {
        bSoftwarePresent = true;
    }
    
    bSaving = false;
}

void ProductModule::draw(float x, float y, float w, float h){
    
    threeDModel.setPosition(ofGetWidth() * 0.5, (float)ofGetHeight() * 0.5 , 0);
    
    ofPushStyle();

    
    ofPushMatrix();
    


    ofEnableDepthTest();
    
    ofPushMatrix();
    
    ofTranslate(threeDModel.getPosition().x + 6000, threeDModel.getPosition().y, -10000);
    ofScale(3.2, 3.2);
    ofTranslate(-threeDModel.getPosition().x, -threeDModel.getPosition().y, 0);
    
    
    
    ofTranslate(threeDModel.getPosition().x, threeDModel.getPosition().y - 350, 0);
    ofRotate(rotDegree, 0, 1, 0);
    if (!bSaving)
        rotDegree+=2;
    ofTranslate(-threeDModel.getPosition().x, -threeDModel.getPosition().y, 0);
    
    
    ofSetColor(190,60);
    threeDModel.drawFaces();

    ofSetColor(0);
    glPointSize(2);
//    threeDModel.drawWireframe();
    threeDModel.drawVertices();

    
    glDisable(GL_CULL_FACE); //SUPER IMPORTANT!!!!!! Without this, normal shapes are not drawn
    ofPopMatrix();
    ofDisableDepthTest();
    
    ofPopMatrix();
    
    ofPopStyle();
}

bool ProductModule::isSoftwarePresent(){
    return bSoftwarePresent;
}