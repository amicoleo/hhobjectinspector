//
//  defines.h
//  hhObjectInspector
//
//  Created by Leonardo Amico on 31/08/2015.
//
//

#include "ofMain.h"





#ifndef hhObjectInspector_defines_h
#define hhObjectInspector_defines_h


const ofColor guiColorBg = ofColor(255,200);;
const ofColor guiColorText = ofColor(0);
const ofColor guiColorTextUnactive = ofColor(172);






//------------------//





#define H_TOPBAR    200*2
#define H_BOTTOMBAR 300*2


#define X_VIDEO 0
#define Y_VIDEO 0
#define W_VIDEO 2048
#define H_VIDEO 1536

#define X_AREA_VIDEO 0
#define Y_AREA_VIDEO 0
#define W_AREA_VIDEO 512.00*2
#define H_AREA_VIDEO 384.000*2
const ofColor areaVideoColor = ofColor(255);

#define X_AREA_STATUS 512.000*2
#define Y_AREA_STATUS 0
#define W_AREA_STATUS 512.000*2
#define H_AREA_STATUS 384.000*2
const ofColor areaStatusoColor = ofColor(255);


#define X_AREA_VISUAL 0
#define Y_AREA_VISUAL 384.000*2
#define W_AREA_VISUAL 512.000*2
#define H_AREA_VISUAL 384.000*2
//const ofColor areaVisualColor = ofColor(0,0,0);


#define X_AREA_TEXTUAL 512.000*2
#define Y_AREA_TEXTUAL 384.000*2
#define W_AREA_TEXTUAL 512.000*2
#define H_AREA_TEXTUAL 384.000*2
//const ofColor areaTextualColor = ofColor(0,0,0);

#define X_MODEBUTTON_3D 512.000*2
#define Y_MODEBUTTON_3D 279.502*2
#define W_MODEBUTTON_3D 170.667*2
#define H_MODEBUTTON_3D 104.499*2
const ofColor modeButton3DColor = ofColor(222,234,237);


#define X_MODEBUTTON_BOM 682.667*2
#define Y_MODEBUTTON_BOM 279.501*2
#define W_MODEBUTTON_BOM 170.667*2
#define H_MODEBUTTON_BOM 104.499*2
const ofColor modeButtonBomColor = ofColor(237,236,222);

#define X_MODEBUTTON_CODE 853.333*2
#define Y_MODEBUTTON_CODE 279.501*2
#define W_MODEBUTTON_CODE 170.667*2
#define H_MODEBUTTON_CODE 104.499*2
const ofColor modeButtonCodeColor = ofColor(227,222,237);








const ofRectangle areaVideo = ofRectangle(X_AREA_VIDEO, Y_AREA_VIDEO, W_AREA_VIDEO, H_AREA_VIDEO);
const ofRectangle areaStatus = ofRectangle(X_AREA_STATUS, Y_AREA_STATUS, W_AREA_STATUS, H_AREA_STATUS);
const ofRectangle areaVisual = ofRectangle(X_AREA_VISUAL, Y_AREA_VISUAL, W_AREA_VISUAL, H_AREA_VISUAL);
const ofRectangle areaTextual = ofRectangle(X_AREA_TEXTUAL, Y_AREA_TEXTUAL, W_AREA_TEXTUAL, H_AREA_TEXTUAL);
const ofRectangle modeButton3D  = ofRectangle(X_MODEBUTTON_3D, Y_MODEBUTTON_3D, W_MODEBUTTON_3D, H_MODEBUTTON_3D);
const ofRectangle modeButtonBom  = ofRectangle(X_MODEBUTTON_BOM, Y_MODEBUTTON_BOM, W_MODEBUTTON_BOM, H_MODEBUTTON_BOM);
const ofRectangle modeButtonCode  = ofRectangle(X_MODEBUTTON_CODE, Y_MODEBUTTON_CODE, W_MODEBUTTON_CODE, H_MODEBUTTON_CODE);







#endif
