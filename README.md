

TODO
------

+ Bugs
    + When no object selected, make the full screen flag false - OK
    + 

+ App tweaks
    + stl black - OK

    JESSE
    + Make texts as transparent
    + Make schematic just b/b - transparent bg
    + make code images

    LEO
    + Grey out the button if there's no software - OK
    + Move image to side of the app (where there is no menu) - OK
    + Make scrolling work for full screen image - OK
    + bigger/more visible X button for full screen - OK

    + bigger 3d model - OK
    
    + I need to be able to scroll vertically when code (now not possible) - OK
    + object blob - OK
        + lighter white
        + thinner border
    + move app title up - OK
    + move button mode down - OK

    + change font status
        + monospace
        + sans serif
    
    + Secret gesture for 
        + Connecting status text...
        + No selection when connecting

    + Make content 

    
    + Colors thresholds tweaking (dark green)
    
    
    
    <!--    + Make schematic with arduino and level converter-->

    
    

    




+ Make buttons work - OK
+ Put content in different sections
    + 3D
        + Model
        + Text
    + Electronics
        + Make it as pdf with textEdit - make it as png with gimp (crop white borders) - OK

    + Software
        + Text (image - from Atom) - scrollable in place/cropped in height

+ Click on it - full screen
    + button for closing it when full screen


+ Circuit appear in the center, full screen when clicking on it
    + can navigate on it/scrolling it (using scrollingArea)
    + it is masked by semitransparent rects
    + it could be that the image has all the white as transparent, so overlays on what behind on the sides, but has a white background behind




+ Pass content to productModule, so it can set dimensions
+ Call scroll view setup



+ Create and put content for different parts

+ Fix bottoms behaviour (toggle mode)
+ choose part with gui

+ Make text block scrolling
+ Status text reacting to events

+ Secret gesture for showing "loading object"
+ secret gesture for showing "saving object"


+ Secret gui
    + setting color filter values - OK
    + reset save button

+ Logic for selecting different part when touching on block
+ Status text showing current selected part


+ Database of different parts


+ Show 3d Model - OK
+ Keep blob selection when moving screen - OK


+ Content for blocks
    + Electronics
        + Circuit
        + BOM
    + Software
        + Code (full screen)



UI


+ Layout

+ Button

+ Some animation for button pushed
+ Do button class



3D Model
----
+ Export with origin at the center
+ Format is ply
+ Check the file type on xCode, if it crashes when loads it (is should be data)
