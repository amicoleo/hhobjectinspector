//
//  ScrollableArea.h
//
//  Created by Leonardo Amico on 03/10/2015.
//
//



#include "ofxPanZoom.h"

class ScrollableArea{
public:
    void setup(ofRectangle _scrollableAreaRect){
        scrollableAreaRect = _scrollableAreaRect;
        cam.setZoom(1.0f);
        cam.setMinZoom(1.0f);
        cam.setMaxZoom(5.0f);
        
        
        cam.setScreenSize( ofGetWidth(), ofGetHeight() ); //tell the system how large is out screen
        
        
        cam.setViewportConstrain(
                                                ofVec3f(-scrollableAreaRect.x, -scrollableAreaRect.y),
                                                ofVec3f(
                                                        scrollableAreaRect.width + (ofGetWidth() - scrollableAreaRect.width) - scrollableAreaRect.x,
                                                        scrollableAreaRect.height + (ofGetHeight() - scrollableAreaRect.height) - scrollableAreaRect.y)
                                                ); //limit browseable area, in world units
        
        cam.lookAt( ofVec2f(ofGetWidth()/2 - scrollableAreaRect.x, ofGetHeight()/2 - scrollableAreaRect.y) );

    }
    void update(){
        cam.update(0.016f);
    }
    
    void touchDown(ofTouchEventArgs &touch){
        if (scrollableAreaRect.inside(touch))
            cam.touchDown(touch); //fw event to scrollableArea.cam
    }
    void touchMoved(ofTouchEventArgs &touch){
        cam.touchMoved(touch);
    }
    void touchUp(ofTouchEventArgs &touch){
        cam.touchUp(touch);
    }
    
    void apply(){
        ofPushStyle();
        ofFill();
        ofSetColor(ofColor::white);
        ofRect(0, 0, ofGetWidth(), ofGetHeight());
        ofPopStyle();
        
        cam.apply();
    }
    
    void reset(){
        cam.reset();
        
        ofPushStyle();
        ofFill();
        ofRect(0, 0, ofGetWidth(), scrollableAreaRect.y);
        ofRect(0, scrollableAreaRect.y + scrollableAreaRect.height, ofGetWidth(), (ofGetHeight() - scrollableAreaRect.y) - scrollableAreaRect.height);
        ofRect(0, 0, scrollableAreaRect.x, ofGetHeight());
        ofRect(scrollableAreaRect.x + scrollableAreaRect.width, 0, (ofGetWidth() - scrollableAreaRect.x) - scrollableAreaRect.width, ofGetHeight());
        ofPopStyle();

    }

    ofxPanZoom	cam;
    ofRectangle scrollableAreaRect;

};

